var client = require('socket.io-client');
var socket = client.connect(window.location.protocol + '//' + window.location.hostname + ':3000');
var request = require('request');

var q = [];
var lastCreatedat = "0";

socket.on('photo', function(msg){
  q.push(msg);
});

setInterval(function() {
  var photo = q.shift();
  if(photo) {
    renderImage(photo.path, photo.orientation);
  } else {
    request.get(window.location.origin + '/photos?newer=' + lastCreatedat, function(error, res, body) {
      var json = JSON.parse(body).photo;
      var now = new Date();
      if(json && json.createdat) {
        lastCreatedat = json.createdat;
      } else {
        lastCreatedat = now.getTime();
      }
      var lastDate = new Date(lastCreatedat);
      if(now.getTime() - 60000 < lastDate.getTime()) {
        // 上映中の時刻の1分前まで過去のデータを上映し終わった場合、日付初期値に戻す
        lastCreatedat = "0";
      }
      renderImage(json.path, json.orientation);
    });
  }
}, 8 * 1000);


function renderImage(url, orientation) {
  var options = {
    orientation: orientation
  };
  loadImage(url, function (img) {
    img.classList.add('slide');
    var frame = document.getElementById('slide-frame');
    frame.removeChild(frame.childNodes[0]);
    frame.appendChild(img);
  }, options);
}

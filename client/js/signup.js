var React = require('react');
var MinLogo = require('./components/logo.js').MinLogo;

React.render(<MinLogo />, document.getElementById('title-frame-min'));

var signUpButton = document.getElementById('sign-up-button');

signUpButton.onclick = function() {

  var email = document.getElementById('sign-up-email').value;
  var nickname = document.getElementById('sign-up-nickname').value;
  var password = document.getElementById('sign-up-password').value;

  if(!email || !nickname || !password) {
    var message = document.getElementById('sign-up-warn');
    message.innerHTML = '必須項目が入力されていません。';
    message.hidden = false;

    return false;
  }

  return true;
};

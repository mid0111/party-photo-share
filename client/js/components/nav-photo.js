var React = require('react');

module.exports = NavPhoto = React.createClass({
  propTypes: {
    enable: React.PropTypes.bool.isRequired,
    direction: React.PropTypes.string.isRequired,
    handleClick: React.PropTypes.func.isRequired
  },
  _handleClick: function() {
    this.props.handleClick(this.props.direction);
  },
  render: function() {
    if(!this.props.enable) {
      return(
        <div></div>
      );
    }
    if(this.props.direction === 'prev') {
      return (
          <a className="cursor page-navigator-right" onClick={this._handleClick}>
          <img className="icon icon-page-navigator-right" src="/img/nav-right.png" onClick={this._handleClick}/>
          </a>
      );
    } else {
      return (
          <a className="cursor page-navigator-left" onClick={this._handleClick}>
          <img className="icon-page-navigator-left" src="/img/nav-left.png"/>
          </a>
      );
    }
  }
});


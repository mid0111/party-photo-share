var React = require('react');
var request = require('request');
var Promise = require('bluebird');

var PhotoList = require('./photo-list');
var PhotoZoomBox = require('./photo-zoom-box');

module.exports = React.createClass({
  propTypes: {
    sortOption: React.PropTypes.string.isRequired,
    isUserMode: React.PropTypes.bool.isRequired
  },

  getInitialState :function() {
    return {
      isZoomMode: false,
      photoZoom: {},
      stars: [],
      photoList: [],
      disableScroll: false
    };
  },

  componentDidMount: function() {
    Promise.all([
      getPhotoList(0, this.props.sortOption, this.props.isUserMode),
      getStarPhotoIds()
    ]).then(function(values) {
      var photoList = this.state.photoList;
      photoList.push({
        index: 0,
        photos: values[0]
      });
      this.setState({
        stars: values[1],
        photoList: photoList
      });
    }.bind(this));
  },

  handleOnStar: function(photo, starCount) {
    for(var listIndex = 0; listIndex < this.state.photoList.length; listIndex ++) {
      var currentList = this.state.photoList[listIndex];
      var currentIndex = findIdFromPhotoList(currentList, photo._id);
      if(currentIndex < 0) continue;

      var newPhotoList = this.state.photoList;
      newPhotoList[listIndex].photos[currentIndex].starCount = starCount;

      var stars = this.state.stars;
      var starIndex = stars.indexOf(photo._id);
      if(starIndex < 0) {
        stars.push(photo._id);
      } else {
        stars.splice(starIndex, 1);
      }

      var zoomInf = this.state.photoZoom;
      zoomInf.starCount = starCount;
      this.setState ({
        photoList: newPhotoList,
        stars: stars,
        photoZoom: zoomInf
      });
    }
  },

  handleScroll: function(next) {
    var index = this.state.photoList[this.state.photoList.length - 1].index + 30;
    getPhotoList(index, this.props.sortOption, this.props.isUserMode).then(function(value) {
      if(!value || value.length == 0) {
        var zoomInf = this.state.photoZoom;
        zoomInf.enableNavRight = false;
        this.setState({
          photoZoom: zoomInf,
          disableScroll: true
        });
        return;
      }

      var photoList = this.state.photoList;
      photoList.push({
        index: index,
        photos: value
      });
      this.setState({
        photoList: photoList
      });

      if(typeof next === 'function') {
        next();
      }
    }.bind(this));
  },

  handleOnZoom: function(photo) {
    photo.enableNavLeft = true;
    photo.enableNavRight = true;
    this.setState({
      isZoomMode: !this.state.isZoomMode,
      photoZoom: photo
    });
  },

  handleOffZoom: function() {
    this.setState({
      isZoomMode: !this.state.isZoomMode
    });
  },

  handleOnPrevZoom: function() {
    for(var listIndex = 0; listIndex < this.state.photoList.length; listIndex ++) {
      var currentList = this.state.photoList[listIndex];
      var currentIndex = findIdFromPhotoList(currentList, this.state.photoZoom._id);
      if(currentIndex < 0) continue;

      var next = getPrevZoomInfo(currentList, currentIndex, listIndex, this.state.photoList);
      if(next) {
        this.setState({
          photoZoom: next
        });
        return;
      }
    }
    // 取得済みの情報から次の情報が取得できなかった場合、サーバから次のリストを取得
    this.handleScroll(this.handleOnPrevZoom);
  },

  handleOnNextZoom: function() {
    for(var listIndex = 0; listIndex < this.state.photoList.length; listIndex ++) {
      var currentList = this.state.photoList[listIndex];
      var currentIndex = findIdFromPhotoList(currentList, this.state.photoZoom._id);
      if(currentIndex < 0) continue;

      var next = getNextZoomInfo(currentList, currentIndex, listIndex, this.state.photoList);
      if(next) {
        this.setState({
          photoZoom: next
        });
        return;
      }
    }
    var zoomInf = this.state.photoZoom;
    zoomInf.enableNavLeft = false;
    this.setState({
      photoZoom: zoomInf
    });
  },

  render: function() {
    if(this.state.isZoomMode) {
      return (
          <PhotoZoomBox
        _id={this.state.photoZoom._id}
        name={this.state.photoZoom.name}
        originalname={this.state.photoZoom.originalname}
        width={this.state.photoZoom.width}
        height={this.state.photoZoom.height}
        author={this.state.photoZoom.author}
        starCount={this.state.photoZoom.starCount}
        stars={this.state.stars}
        enableNavLeft={this.state.photoZoom.enableNavLeft}
        enableNavRight={this.state.photoZoom.enableNavRight}
        handleOnPrevZoom={this.handleOnPrevZoom}
        handleOnNextZoom={this.handleOnNextZoom}
        handleOffZoom={this.handleOffZoom}
        handleOnStar={this.handleOnStar}
          />
      );
    } else {
      var photoList = this.state.photoList.map(function(item) {
        return (
            <PhotoList
          index={item.index}
          photos={item.photos}
          stars={this.state.stars}
          key={item.index}
          onZoom={this.handleOnZoom}
          handleOnStar={this.handleOnStar}
            />
        );
      }.bind(this));
      var buttonClass = 'pure-button button button-scroll';
      if(this.state.disableScroll) {
        buttonClass += ' pure-button-disabled';
      }
      return (
          <div>
          {photoList}
          <button className={buttonClass} onClick={this.handleScroll}>もっとみる</button>
          </div>
      );
    }
  }
});


function getPhotoList(index, sort, isUserMode) {
  if(isUserMode) {
    return new Promise(function(onFulfilled, onRejected) {
      request.get(window.location.origin + '/users/star?show=' + index, function(error, res, body) {
        return onFulfilled(JSON.parse(body).photos);
      });
    });
  }

  return new Promise(function(onFulfilled, onRejected) {
    request.get(window.location.origin + '/photos?show=' + index + '&sort=' + sort, function(error, res, body) {
      return onFulfilled(JSON.parse(body).photos);
    });
  });
}

function getStarPhotoIds() {
  return new Promise(function(onFulfilled, onRejected) {
    request.get(window.location.origin + '/users', function(error, res, body) {
      return onFulfilled(JSON.parse(body).user.star);
    });
  });
}

function parsePhotoResponse(body, currentPhoto, isLeftEnd) {
  var enableNav = JSON.parse(body).photo ? true : false;
  var photo = currentPhoto;
  if(enableNav) {
    photo = JSON.parse(body).photo;
  }
  if(isLeftEnd) {
    photo.enableNavLeft = enableNav;
    photo.enableNavRight = true;
  } else {
    photo.enableNavLeft = true;
    photo.enableNavRight = enableNav;
  }

  return photo;
}

function findIdFromPhotoList(list, id) {
  for(var i = 0; i < list.photos.length; i++) {
    if(id === list.photos[i]._id) {
      return i;
    }
  }
  return -1;
}

function getPrevZoomInfo(currentList, currentIndex, listIndex, list) {
  var next;

  if(currentIndex < currentList.photos.length - 1) {
    // カレントのリストをフェッチして情報を取得
    next = currentList.photos[currentIndex + 1];
    next.enableNavLeft = true;
    next.enableNavRight = true;
    return next;
  }

  if(listIndex < list.length - 1) {
    // 次のリストから情報を取得
    var photos = list[listIndex + 1].photos;
    next = photos[0];
    next.enableNavLeft = true;
    next.enableNavRight = true;
    return next;
  }
  return next;
}

function getNextZoomInfo(currentList, currentIndex, listIndex, list) {
  var next;

  if(currentIndex > 0) {
    // カレントのリストをフェッチして情報を取得
    next = currentList.photos[currentIndex - 1];
    next.enableNavLeft = true;
    next.enableNavRight = true;
    return next;
  }

  if(listIndex > 0) {
    // 次のリストから情報を取得
    var photos = list[listIndex - 1].photos;
    next = photos[photos.length - 1];
    next.enableNavLeft = true;
    next.enableNavRight = true;
    return next;
  }
  return next;
}

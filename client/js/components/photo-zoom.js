var React = require('react');
var joinClasses = require('react/lib/joinClasses');

module.exports = PhotoZoom = React.createClass({
  propTypes: {
    url: React.PropTypes.string.isRequired,
    originalname: React.PropTypes.string.isRequired,
    width: React.PropTypes.number.isRequired,
    height: React.PropTypes.number.isRequired
  },

  getInitialState: function(){
    return {
      loaded: false,
      img: null
    };
  },

  onImageLoad: function() {
    if (this.isMounted()) {
      this.setState({loaded: true});
    }
  },

  componentWillReceiveProps: function(nextProps) {
    if (this.isMounted() && nextProps.url !== this.props.url) {
      this.setState({loaded: false});
      this.state.img.src = nextProps.url;
    }
  },

  componentDidMount: function() {
    this.setState({loaded: false});
    var imgTag = this.refs.img.getDOMNode();
    var imgSrc = imgTag.getAttribute('src');
    var img = new window.Image();
    img.onload = this.onImageLoad;
    img.src = imgSrc;
    this.setState({img: img});
  },

  render: function() {
    var className = 'image photo-zoom';
    if(this.state.loaded) {
      className = joinClasses(className, 'image-loaded');
    }
    return (
        <img ref="img" className={className} src={this.props.url} title={this.props.originalname} />
    );
  }
});


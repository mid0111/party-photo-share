process.env.PORT = '3001';
process.env.BLOB_PATH = './test/uploads/';
process.env.MONGO_HOST = process.env.WERCKER_MONGODB_HOST || require('../config/config.js').db.host;

module.exports = app = require('../app');


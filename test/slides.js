var request = require('supertest');
var express = require('express');
var fs = require('fs');
var expect = require('chai').expect;

var blobPath = __dirname + '/uploads/';

var app = require('./app');
var passportStub = require('passport-stub');

describe('GET /slides', function() {

  before(function() {
    passportStub.install(app);
    passportStub.login({username: 'john.doe'});
  });

  it('スライド画面が表示されること', function(done) {
    request(app)
      .get('/slides')
      .expect(200)
      .expect('Content-Type', 'text/html; charset=utf-8')
      .end(function(err, res){
        if (err) throw err;
        done();
      });
  });
});

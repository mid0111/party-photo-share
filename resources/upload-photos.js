#!/usr/bin/env node

var request = require('request');
var fs = require('fs');

var rootDir = '/Users/mid/Downloads/';

fs.readdir(rootDir, function(err, files){
  if (err) throw err;
  var fileList = [];
  files.filter(function(file){
    console.log('1 -- ' + file);
    return fs.statSync(rootDir + file).isFile() && /.*\.JPG$/.test(rootDir + file);
  }).forEach(function (file) {
    console.log('2 -- ' + file);
    fileList.push(rootDir + file);
  });
  console.log(fileList);

  // authorization
  authorization().then(function(cookie) {
    // upload start
    Promise.all(fileList.map(function(file) {
      uploadFile(file, cookie);
    })).then(function() {
      console.log('Completed!!');
    }, function(err) {
      console.error('Failed to upload. ' + err);
    });
  });
});


function authorization() {
  return new Promise(function(onFulfilled, onRejected) { 
    request.post({
      url: 'http://localhost:3000/auth/login',
      formData: {
        email: 'm.tajima0111@gmail.com',
        password: '001500'
      }
    }, function(error, res, body) {
      if(error) {
        return onRejected(error);
      }
      console.log('3 -- ' + res.headers['set-cookie']);
      return onFulfilled(res.headers['set-cookie']);
    });
  });
}

function uploadFile(path, cookie) {
  return new Promise(function(onFulfilled, onRejected) {
    var formData = {
      files: fs.createReadStream(path)
    };
    request.post({
      url: 'http://localhost:3000/upload',
      formData: formData,
      headers: {
        cookie: cookie
      }
    }, function(error, res, body) {
      if(error) {
        return onRejected(error);
      }
      console.log(res.statusCode);
      return onFulfilled(body);
    });
  });
}

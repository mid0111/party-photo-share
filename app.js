var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var config = require('./config/config');
var authConfig = require('./config/auth.js');

console.log(JSON.stringify(config, null, 2));

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

// mongoose
var mongoose = require('mongoose');
var mongoUrl = 'mongodb://' + config.db.host + '/';
var dbName = config.db.dbName;
mongoose.connect(mongoUrl + dbName);
require('./models/photo');
require('./models/user');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon(__dirname + '/build/img/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Session
var MongoStore = require('connect-mongo')(session);
app.use(session({
  secret: authConfig.session.secret,
  store: new MongoStore({ mongooseConnection: mongoose.connection }),
  resave: false,
  saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

// authorization
require('./auth');

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'build')));

/// routing
var routes = require('./routes/index');
var upload = require('./routes/upload');
var photos = require('./routes/photos');
var slides = require('./routes/slides');
var users = require('./routes/users');
var auth = require('./routes/auth');
app.use(authChecker);
app.use('/', routes);
app.use('/upload', upload);
app.use('/photos', photos);
app.use('/slides', slides);
app.use('/users', users);
app.use('/auth', auth);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    console.error('Internal Server Error ' + err);
    console.trace();
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  console.error('Internal Server Error ' + err);
  console.trace();
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// Settings for socket.io
io.on('connection', function(socket){
  console.log('user connected');
  socket.on('photo', function(msg) {
    socket.broadcast.emit('photo', msg);
  });
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

app.set('port', config.app.port);

var server = http.listen(3000, function(){
  console.log('Express server listening on port ' + server.address().port);
});

module.exports = app;


function authChecker(req, res, next) {
  if (req.user 
      || req.path.match(/^\/auth.*/)
      || req.path === '/') {
    next();
  } else {
    res.redirect('/');
  }
}

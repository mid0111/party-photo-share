var gulp = require('gulp');
var util = require('gulp-util');
var gulpif = require('gulp-if');

var nodemon = require('gulp-nodemon');
var browserSync = require('browser-sync');
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');
var reactify = require('reactify');
var source = require('vinyl-source-stream');
var concat = require('gulp-concat');
var minifyCSS = require('gulp-minify-css');
var rename = require('gulp-rename');

var mocha = require('gulp-mocha');
var bower = require('main-bower-files');
var fs = require('fs');

function reload() {
  browserSync.reload({ stream: false });
};

gulp.task('fonts', function() {
  return gulp.src('./bower_components/components-font-awesome/fonts/**')
    .pipe(gulp.dest('./build/fonts'));
});

gulp.task('css', function() {
  var css = bower({filter: /\.css/i});
  // main bower に対応していないモジュール
  css.push('bower_components/pure/grids-responsive-min.css');
  // original ソース
  css.push('client/css/*.css');

  return gulp.src(css)
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./build/css'))
        .pipe(minifyCSS())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('./build/css'));
});

gulp.task('img', function() {
  return gulp.src('client/img/*.{png,jpg,jpeg,ico}')
    .pipe(gulp.dest('build/img'));
});

gulp.task('scripts', function() {
  gulp.src('./client/js/vendor/*.js')
    .pipe(gulp.dest('./build/js/vendor'));

  return gulp.src('./client/js/*.js') 
    .pipe(browserify({
      debug: !(process.env.NODE_ENV === 'production'),
      transform: [reactify]
    }))
    .pipe(gulpif(process.env.NODE_ENV === 'production', uglify({mangle: false})))
    .pipe(gulp.dest('./build/js/'));
});

gulp.task('watch-css', ['css'], function() {
  reload();
});

gulp.task('watch-scripts', ['scripts'], function() {
  reload();
});

gulp.task('watch-img', ['img'], function() {
  reload();
});

gulp.task('test', function() {
  return gulp.src('./test/**/*.js')
    .pipe(mocha())
    .once('end', function () {
      process.exit();
    });
});

gulp.task('browsersync', function() {
  browserSync.init({
    files: ['build/img/*', 'views/**/*.*'],
    proxy: 'http://localhost:3000',
    port: 4000,
    browser: ['google chrome'],
    open: false
  });
});

gulp.task('serve', ['build', 'browsersync'], function () {
  nodemon({
    script: './app',
    ext: 'js',
    ignore: [
      'node_modules',
      'bower_components',
      'bin',
      'views',
      'client',
      'build',
      'test'
    ],
    env: {
      'NODE_ENV': 'development',
      'DEBUG': 'party-photo-share'
    },
    stdout: false
  }).on('readable', function() {
    this.stdout.on('data', function(chunk) {
      if (/^Express\ server\ listening/.test(chunk)) {
        reload();
      }
      process.stdout.write(chunk);
    });
    this.stderr.on('data', function(chunk) {
      process.stderr.write(chunk);
    });
  });

  gulp.watch('./client/js/**/*', ['watch-scripts']);
  gulp.watch('./client/css/*', ['watch-css']);
  gulp.watch('./client/img/*', ['watch-img']);
});

gulp.task('build', ['scripts', 'css', 'img', 'fonts']);
gulp.task('default', ['serve']);

var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Photo = mongoose.model('Photo');
var User = mongoose.model('User');

router.get('/', function(req, res) {
  console.log('query: ' + JSON.stringify(req.query));
  var showIndex = req.query.show;
  var sort = req.query.sort;
  var newer = req.query.newer;

  if(showIndex) {
    // 一覧取得
    list(sort, showIndex).exec().then(function(docs) {
      res.send({
        photos: docs
      });
    }, function(err) {
      console.error('Failed to find metadata of photos. ' + err);
      res.status(404).send();
    });
    return;
  }

  if(newer) {
    // 1つ後の写真を検索
    correctPhotoNewer(newer).then(function(docs){
      res.send({
        photo: docs[0]
      }), function(err) {
        console.error('Failed to find metadata of newer photo. ' + err);
      };
    });
    return;
  }

  // 一覧画面描画
  res.render('photos', {
    sort: sort
  });
});



router.put('/star/countup', function(req, res) {
  console.log('query: ' + JSON.stringify(req.query));
  var id = req.query.id;
  var userId = req.user._id;

  Promise.all([
    updateStarCount(id, 1),
    StarPhoto(userId, id)
  ]).then(function() {
    console.log('Completed to update star count.');
    res.status(204).send();
  }, function(err) {
    console.log('Failed to update star count. ' + err);
    res.status(400).send();
  });
});

router.put('/star/countdown', function(req, res) {
  console.log('query: ' + JSON.stringify(req.query));
  var id = req.query.id;
  var userId = req.user._id;

  Promise.all([
    updateStarCount(id, -1),
    UnstarPhoto(userId, id)
  ]).then(function() {
    console.log('Completed to update star count.');
    res.status(200)
      .send();
  }, function(err) {
    console.log('Failed to update star count. ' + err);
    res.status(400).send();
  });
});

function list(sortKey, skip) {
  var sort = sortKey;
  if(sort !== 'createdat') {
    sort += ' -createdat';
  }
  console.log('sort: ' + sort);

  return Photo.find({})
    .populate('author', 'name')
    .sort('-' + sort)
    .limit(30)
    .skip(skip);
};

function StarPhoto(userId, photoId) {
  console.log('Updating user info...');
  return new Promise(function(onFulfilled, onRejected) {
    return User.findById(userId)
      .exec()
      .then(function(doc) {
        if(doc.star.indexOf(photoId) >= 0) {
          console.log('User already star the photo. [' + photoId + ']');
          return onFulfilled(doc);
        }
        doc.star.push(photoId);
        doc.save(function() { 
          console.log('Updating user info complete.');
          return onFulfilled(doc);
        });
      }, function(err) {
        console.error('Failed to search user info for merge request. ' + err);
        return onRejected(err);
      });
  });
}

function UnstarPhoto(userId, photoId) {
  console.log('Updating user info...');
  return new Promise(function(onFulfilled, onRejected) {
    return User.findById(userId)
      .exec()
      .then(function(doc) {
        var index = doc.star.indexOf(photoId);
        if(index < 0) {
          console.log('User already unstar the photo. [' + photoId + ']');
          return onFulfilled(doc);
        }
        doc.star.splice(index, 1);
        doc.save(function() { 
          console.log('Updating user info complete.');
          return onFulfilled(doc);
        });
      }, function(err) {
        console.error('Failed to search user info for merge request. ' + err);
        return onRejected(err);
      });
  });
}

function updateStarCount(id, operation) {
  console.log('Updating photo info...');
  return new Promise(function(onFulfilled, onRejected) {
    return Photo.findById(id)
      .exec()
      .then(function(doc) {
        console.log(doc);
        doc.starCount = doc.starCount + operation;
        doc.save(function() { 
          console.log('Updating photo info complete.');
          return onFulfilled(doc);
        });
      }, function(err) {
        console.error('Failed to search documents for merge request. ' + err);
        return onRejected(err);
      });
  });
}

function correctPhotoNewer(createdat) {
  return Photo.find()
    .where('createdat')
    .gt(createdat)
    .populate('author', 'name')
    .sort({'createdat':1})
    .limit(1)
    .exec();
}

module.exports = router;

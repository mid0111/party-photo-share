var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  if(!req.user) {
    return res.render('signin');
  }

  return res.render('home');
});

module.exports = router;

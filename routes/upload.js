var express = require('express');
var router = express.Router();
var ExifImage = require('exif').ExifImage;
var mongoose = require('mongoose');
var lwip = require('lwip');
var Photo = mongoose.model('Photo');
var config = require('../config/config');
var io = require('socket.io-client');
var socket = io.connect(config.app.scheme + '://' + config.app.host + ':' + config.app.port);

// multer settings
var multer  = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, config.blob.path);
  },
  filename: function (req, file, cb) {
    cb(null, require('node-uuid').v4() + '.jpg');
  }
});
var upload = multer({ storage: storage });

router.post('/', upload.array('files'), function(req, res, next) {
  console.log('Processing on uploaded file.');
  req.files.forEach(function(file) {
    file.dir = file.path.replace(/^(.*)\/org\/.*$/, '$1');
    analyzeOrientation(file).then(function(file) {
      Promise.all([
        createThumb(file),
        resize(file)
      ]).then(function(data) {
        save(data[0], req.user._id);
      });

      // socket にイベント通知
      socket.emit('photo', {
        path: file.path.replace('build/', ''),
        originalname: file.originolname,
        orientation: file.orientation
      });
    });
  });

  // とりあえず、元のページに戻っとく
  res.redirect('/');
});

function resize(file) {
  return new Promise(function(onFulfilled, onRejected) {
    console.log('Resizing uploaded file.');

    // ファイル変換
    lwip.open(file.path, function(err, image){
      if(err) {
        console.error('Failed to resize. ' + err);
        return onRejected(err);
      }

      var width, height;
      if (file.orientation > 4) {
        width = file.width = image.height();
        height = file.height = image.width();
      } else {
        width = file.width = image.width();
        height = file.height = image.height();
      }

      var scale;
      if(width < height) {
        scale = 768 / parseFloat(height);
      } else {
        scale = 768 / parseFloat(width);
      }

      var batch = image.batch();
      if(file.axes) {
        batch.mirror(file.axes);
      }
      if(file.rotate) {
        batch.rotate(file.rotate);
      }
      batch.scale(scale)
        .writeFile(file.dir + '/m/' + file.filename, function(err){
          if(err) {
            console.error('Failed to resize. ' + err);
            return onRejected(err);
          } else {
            console.log('Completed to resize.');
            return onFulfilled();
          }
        });
    });
  });
}

function createThumb(file) {
  return new Promise(function(onFulfilled, onRejected) {
    console.log('Creating to Thumbnail.');
    // ファイル変換
    lwip.open(file.path, function(err, image){
      if(err) {
        console.error('Failed to create Thumbnail. ' + err);
        return onRejected(err);
      }
      var width, height;
      if (file.orientation > 4) {
        width = file.width = image.height();
        height = file.height = image.width();
      } else {
        width = file.width = image.width();
        height = file.height = image.height();
      }

      var left, top, right, bottom, scale;
      if(width < height) {
        scale = 110 / parseFloat(width);
        left = 5;
        top = 0;
        right = 105;
        bottom = 100;
      } else {
        scale = 200 / parseFloat(height);
        var margin =(width * scale - 200) / 2;
        left = margin;
        top = 0;
        right = 200 + margin;
        bottom = 200;
      }

      var batch = image.batch();
      if(file.axes) {
        batch.mirror(file.axes);
      }
      if(file.rotate) {
        batch.rotate(file.rotate);
      }
      batch.scale(scale)
        .crop(left, top, right, bottom)
        .writeFile(file.dir + '/th/' + file.filename, function(err){
          if(err) {
            console.error('Failed to create thumbnail. ' + err);
            return onRejected(err);
          } else {
            console.log('Completed to create thumbnail.');
            return onFulfilled(file);
          }
        });
    });
  });
}

function save(data, userId) {
  return new Promise(function(onFulfilled, onRejected) {
    console.log('Storing metadata to db.');
    var photo = new Photo();
    photo.name = data.filename;
    photo.path = data.path.replace('build/', '');
    photo.originalname = data.originalname;
    photo.mimetype = data.mimetype;
    photo.createdat = new Date();
    photo.width = data.width;
    photo.height = data.height;
    photo.orientation = data.orientation;
    photo.starCount = 0;
    photo.author = userId;
    return photo.save(function (err) {
      if(err) {
        console.error('Failed to store metadata.');
        return onRejected(err);
      } else {
        console.log('Completed to store metadata.');
        return onFulfilled(data);
      }
    });
  });
}

function analyzeOrientation(file) {
  return new Promise(function(onFulfilled, onRejected) {
    var converted = file;
    try {
      new ExifImage({ image : file.path }, function (err, exifData) {
        if (err) {
          console.error('Failed to load exif. ' + err);
          return onRejected(err);
        }
        var orientation = converted.orientation = exifData.image.Orientation;
        if (!orientation || orientation > 8) {
          return onFulfilled(converted);
        }

        switch (orientation) {
        case 2:
          // horizontal flip
          converted.axes = 'x';
          break;
        case 3:
          // 180° rotate left
          converted.rotate = -180;
          break;
        case 4:
          // vertical flip
          converted.axes = 'y';
          break;
        case 5:
          // vertical flip + 90 rotate right
          converted.axes = 'y';
          converted.rotate = 90;
          break;
        case 6:
          // 90° rotate right
          converted.rotate = 90;
          break;
        case 7:
          // horizontal flip + 90 rotate right
          converted.axes = 'x';
          converted.rotate = 90;
          break;
        case 8:
          // 90° rotate left
          converted.rotate = -90;
          break;
        }
        return onFulfilled(converted);
      });
    } catch (error) {
      console.error('Failed to load exif. ' + error);
    }
  });
};

module.exports = router;

module.exports = {
  app: {
    scheme: 'http',
    host: process.env.HOST || '192.168.0.3',
    port: process.env.PORT || 3000
  },

  db: {
    host: process.env.MONGO_HOST || '192.168.99.100',
    dbName: process.env.MONGO_DB || 'party_photo_share'
  },

  blob: {
    path: process.env.BLOB_PATH || './build/img/uploads/org/'
  }
};


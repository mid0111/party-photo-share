module.exports = {
  facebook: {
    appId: 'xxxxxx',
    appSecret: 'xxxxxx'
  },
  twitter: {
    consumerKey: 'xxxxx',
    consumerSecret: 'xxxxx'
  },
  github: {
    clientId: 'xxxxx',
    clientSecret: 'xxxxx'
  },
  app: {
    salt: 10
  },
  session: {
    secret: 'xxxxx'
  }
};

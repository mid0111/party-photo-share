var mongoose = require('mongoose');
var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
var GithubStrategy = require( 'passport-github' ).Strategy;
var LocalStrategy = require('passport-local').Strategy;

var authConfig = require('./config/auth.js');
var config = require('./config/config.js');

var User = mongoose.model('User');

passport.use(new FacebookStrategy({
  clientID: authConfig.facebook.appId,
  clientSecret: authConfig.facebook.appSecret,
  callbackURL: config.app.scheme + '://' + config.app.host + ':' + config.app.port + '/auth/facebook/callback',
  enableProof: false
}, function(accessTokon, refreshToken, profile, done) {
  var user = new User();
  user.facebookId = profile.id;
  user.name = profile.displayName;
  user.imgUrl = 'http://graph.facebook.com/' + profile.id + '/picture';
  
  findOrCreate(user, done);
}));

passport.use(new TwitterStrategy({
  consumerKey: authConfig.twitter.consumerKey,
  consumerSecret: authConfig.twitter.consumerSecret,
  callbackURL: config.app.scheme + '://' + config.app.host + ':' + config.app.port + '/auth/twitter/callback'
}, function(token, tokenSecret, profile, done) {
  var user = new User();
  user.twitterId = profile.id;
  user.name = profile.username;
  user.imgUrl = profile._json.profile_image_url;
  
  findOrCreate(user, done);
}));


passport.use(new GithubStrategy({
  clientID: authConfig.github.clientId,
  clientSecret: authConfig.github.clientSecret,
  callbackURL: config.app.scheme + '://' + config.app.host + ':' + config.app.port + '/auth/github/callback'
}, function(request, accessToken, refreshToken, profile, done) {
  var user = new User();
  user.githubId = profile.id;
  user.name = profile.username;
  user.imgUrl = profile._json.avatar_url;

  findOrCreate(user, done);
}));

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, function(username, password, done) {
  User.findOne({originalId: username}, function(err, user) {
    if (err) {
      console.error('Failed to search user. ' + err);
      return done(err);
    }
    if (!user) {
      console.log('Local Strategy: user not registered.');
        return done(null, false, {
        message: 'Incorrect email.'
      });
    }
    user.comparePassword(password, function(err, isMatch){
      if(err) {
        console.error('Failed to check password. ' + err);
        return done(err);
      }
      if(!isMatch) {
        return done(null, false, {
          message: 'Incorrect password.'
        });
      }
      return done(null, user);
    });
  });
}));

function findOrCreate(json, done) {

  var query = null;
  if(json.facebookId) {
    query = {facebookId: json.facebookId};
  } else if (json.twitterId) {
    query = {twitterId: json.twitterId};
  } else if (json.githubId) {
    query = {githubId: json.githubId};
  }

  User.findOne(query).exec()
    .then(function(user) {
      if(user) {
        console.log('User already registered.');
        return done(null, user);
      }
      console.log('Regist user. [' + JSON.stringify(json) + ']');
      json.save(function(err) {
        if(err) {
          console.error('Failed to regist user. ' + err);
          return done(err);
        }
        console.log('Regist completed.');
        return done(null, json);
      });
    }, function(err) {
      console.error('Failed to search user. ' + err);
      return done(err);
    });
}

passport.serializeUser(function(user, done) {
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});

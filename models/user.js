var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');
var authConfig = require('../config/auth');

var userSchema = new Schema({
  name: String,
  originalId: String,
  hashed_password: String,
  salt: String,
  facebookId: String,
  twitterId: String,
  githubId: String,
  imgUrl: String,
  star: [{type: Schema.Types.ObjectId, ref: 'Photo'}]
});

userSchema
  .virtual('password')
  .set(function(password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashed_password = this.encryptPassword(password);
  })
  .get(function() { return this._password; });

userSchema.methods.comparePassword = function(candidatePassword, cb) {
  var isMatch = (this.encryptPassword(candidatePassword) === this.hashed_password);
  cb(null, isMatch);
};

userSchema.methods.makeSalt = function () {
  return Math.round((new Date().valueOf() * Math.random())) + authConfig.app.salt;
};

userSchema.methods.encryptPassword = function (password) {
  if (!password) return '';
  try {
    return crypto
      .createHmac('sha1', this.salt)
      .update(password)
      .digest('hex');
  } catch (err) {
    return '';
  }
};

var User = mongoose.model('User', userSchema);
